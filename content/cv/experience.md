## Provedores de Cloud
* Google Cloud
* AWS (Main)
* Digital Ocean

## Infrastructure como código
* Terraform

## Integração contínua / Entrega
* GitHub
* Bitbucket
* Jenkins
* Travis
* Bamboo
* GitLab

## Containers
* Docker
* Docker Swarm
* Kubernetes

## Proxy
* Nginx como Proxy reverso
* Squid
* Varnish

## Domains
* Samba
* LDAP - integração em alguns projetos.

## DNS
* Bind
* Route 53 (AWS)

## Databases
* MYSQL/MariaDB
* PostgreSQL

## Email
* Postfix
* iRedMail

## Monitoramento
* ELK (Elasticsearch, Logstash, Kibana)
* Nagios
* Cacti
* SolarWinds

## Métricas
* InfluxDB
* Telegraf
* Grafana

Stack usando Docker Compose.

## Gerenciamento de configuração e orquestração
* Puppet
* Ansible

## Virtualização
* VMWare Server
* VMWare Esxi
* VirtualBox
* Vagrant
* XEN Server
* OpenVirt

## Linguagens
* PHP - intermediate
* Shell Script - advanced
* Ruby - basic
* Python - básico